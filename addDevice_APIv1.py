#!/usr/bin/python

import csv
import json
import requests
import time
import getopt
import sys


user = ""
pwd = ""
appid = ""
csvfile = ""

#print('ARGV      :', sys.argv[1:])
options, remainder = getopt.getopt(sys.argv[1:], 'u:p:a:f:h', ['user=', 
                                                               'password=',
                                                               'appid=',
                                                               'csvfile=',
                                                               'help'
                                                         ])
#print('OPTIONS   :', options)

for opt, arg in options:
    if opt in ('-u', '--user'):
        user = arg
    elif opt in ('-p', '--password'):
        pwd = arg
    elif opt in ('-a', '--appid'): 
        appid = arg
    elif opt in ('-f', '--csvfile'):
        csvfile = arg
    elif opt in ('-h', '--help'):
        print("addDevice_APIv1.py is useful to provisioning the device in the ptNetSuite through the API REST v1")
        print("usage: python addDevice_APIv1.py --user <user> --password <password> --appid <appidNumber> --csvfile </path/to/csvFile.csv>")
        print("options:")
        print("\t-u --user\tusername used to login into the ptNetSuite")
        print("\t-p --pasword\tpassword  used to login into the ptNetSuite")
        print("\t-a --appid\tappid where you wanto to add the devices")
        print("\t-f --csvfile\tcsv file that contains the devices")
        print("\t-h --help\tthis help")
        exit(0)

print('USER    :', user)
print('PWD     : ***')
print('APPID   :', appid)
print('CSVFILE :', csvfile)

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/auth/login"
API_ENDPOINT_ADD_DEVICES = "https://" + API_ENDPOINT_ROOT + "/api/mqtt/v1/users/" + user + "/apps/" + appid + "/devices"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

ABP = 0
OTAA = 128

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["accessToken"]
print(headers)


time.sleep(1)
#id|macVersion|supports32bitFCnt|supportsClassB|supportsClassC|
#--|----------|-----------------|--------------|--------------|
# 1|1.0.2     |false            |false         |false         |
# 2|1.0.2     |true             |false         |false         |
# 3|1.0.2     |false            |false         |true          |
# 4|1.0.2     |true             |false         |true          |
# 5|1.0.3     |false            |false         |false         |
# 6|1.0.3     |true             |false         |false         |
# 7|1.0.3     |false            |true          |false         |
# 8|1.0.3     |true             |true          |false         |
# 9|1.0.3     |false            |false         |true          |
#10|1.0.3     |true             |false         |true          |

print("== ADD DEVICES from APPID " + appid + " ==")



# open the CVS file
with open(csvfile, 'rt') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='\'')
    head = True
    for row in spamreader:
        # jump the first row
        if head:
            head = False
            continue

        body = {
            "deveui": "",
            "type": 0,
            "profile": 1,
            "label": "",
            "keys": {},
            "info": {}
}

        #print ', '.join(row)
        serial = row[0]
        devEui = row[1]
        devAdr = row[2]
        labelId = row[3]
        firmware = row[4]
        model = row[5]
        nwkSKey = row[6]
        appSKey = row[7]
        appKey = row[8]
        devProfile = row[9]
        devType = row[10]
    
        #print(devEui, devAdr, labelId, firmware, nwkSKey, appSKey)

        body["deveui"] = devEui
        body["type"] = int(devType)
        body["profile"] = devProfile
        body["label"] = labelId
        if int(devType) == ABP:
            body["devaddr"] = devAdr
            body["keys"]["nwkSKey"] = nwkSKey
            body["keys"]["appSKey"] = appSKey
        elif int(devType) == OTAA:
            body["keys"]["appKey"] = appKey
        else:
            print("[errore]: devType")

        body["info"]["firmware"] = firmware
        body["info"]["model"] = model

        print(json.dumps(body))
        r = requests.post(url=API_ENDPOINT_ADD_DEVICES, headers=headers, data=json.dumps(body))
        print(r.text)
        time.sleep(0.2)
