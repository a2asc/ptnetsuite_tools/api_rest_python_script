# getApps APIv1
## Description
getApps_APIv1.py is useful to collect the Applications in the ptNetSuite through the API REST v1

## usage
```
python getApps_APIv1.py --user <user> --password <password>
```

## options
```
python getApps_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -h --help       this help
```


# addApps APIv1
## Description
addApps_APIv1.py is useful to provisioning the application in the ptNetSuite through the API REST v1

## usage
```
python addApps_APIv1.py --user <user> --password <password> --csvfile </path/to/csvFile.csv>
```

## options
```
python addApps_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -f --csvfile    csv file that contains the apps
        -h --help       this help
```


# removeApps APIv1
## Description
removeApp_APIv1.py is useful to remove the applications in the ptNetSuite through the API REST v1

## usage
```
python removeApp_APIv1.py --user <user> --password <password> --csvfile </path/to/csvFile.csv>
```

## options
```
python removeApp_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -f --csvfile    csv file that contains the apps
        -h --help       this help
```

# AppList.csv
## Description
the appList.csv has the following structure
|appid|         joineui|  label|
|-----|----------------|-------|
|    1|0000000000000001|Label_1|
|    2|0000000000000002|Label_2|

## schema
|       key|            description|
|----------|-----------------------|
|     appid|         application ID|
|   joinEUI|                joinEUI|
|     label|                  label|

# getDevice APIv1
## Description
getDevice_APIv1.py is useful to collect the devices in the ptNetSuite through the API REST v1

## usage
```
python getDevice_APIv1.py --user <user> --password <password> --appid <appidNumber>
```

## options
```
python getDevice_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -a --appid      appid where you wanto to add the devices
        -h --help       this help
```


# addDevice APIv1
## Description
addDevice_APIv1.py is useful to provisioning the devices in the ptNetSuite through the API REST v1

## usage
```
python addDevice_APIv1.py --user <user> --password <password> --appid <appidNumber> --csvfile </path/to/csvFile.csv>
```

## options
```
python addDevice_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -a --appid      appid where you wanto to add the devices
        -f --csvfile    csv file that contains the devices
        -h --help       this help
```


# removeDevice APIv1
## Description
removeDevice_APIv1.py is useful to remove the devices in the ptNetSuite through the API REST v1

## usage
```
python removeDevice_APIv1.py --user <user> --password <password> --appid <appidNumber> --csvfile </path/to/csvFile.csv>
```

## options
```
python removeDevice_APIv1.py --help
        -u --user       username used to login into the ptNetSuite
        -p --pasword    password  used to login into the ptNetSuite
        -a --appid      appid where you wanto to add the devices
        -f --csvfile    csv file that contains the devices
        -h --help       this help
```

# deviceList.csv
## Description
the deviceList.csv has the following structure

|matricola|          devEUI| devAddr|labelID|   firmware|  model|                       nwk_s_key|                       app_s_key|                         app_key|devProfile|devType|
|---------|----------------|--------|-------|-----------|-------|--------------------------------|--------------------------------|--------------------------------|----------|-------|
|     0001|0000000000000001|00000001|Label_1|firmware_xx|modelxx|00000000000000000000000000000001|00000000000000000000000000000001|                          _null_|         2|      0|
|     0002|0000000000000002|  _null_|Label_2|firmware_xx|modelxx|                          _null_|                          _null_|00000000000000000000000000000002|         2|    128|


## schema
|       key|            description|
|----------|-----------------------|
| matricola|          serial number|
|    devEUI|                 devEUI|
|   devAddr|         device address|
|   labelID|                  label|
|  firmware|       firmware version|
|     model|          model version|
| nwk_s_key|    network session key|
| app_s_key|application session key|
|   app_key|        application key|
|devProfile|         device profile|
|   devType|            device type|


## devProfile
|id|macVersion|supports32bitFCnt|supportsClassB|supportsClassC|
|--|----------|-----------------|--------------|--------------|
| 1|1.0.2     |false            |false         |false         |
| 2|1.0.2     |true             |false         |false         |
| 3|1.0.2     |false            |false         |true          |
| 4|1.0.2     |true             |false         |true          |
| 5|1.0.3     |false            |false         |false         |
| 6|1.0.3     |true             |false         |false         |
| 7|1.0.3     |false            |true          |false         |
| 8|1.0.3     |true             |true          |false         |
| 9|1.0.3     |false            |false         |true          |
|10|1.0.3     |true             |false         |true          |
