#!/usr/bin/python

import csv
import json
import requests
import time
import getopt
import sys


user = ""
pwd = ""
appid = ""
csvfile = ""

#print('ARGV      :', sys.argv[1:])
options, remainder = getopt.getopt(sys.argv[1:], 'u:p:a:f:h', ['user=', 
                                                               'password=',
                                                               'appid=',
                                                               'csvfile=',
                                                               'help'
                                                         ])
#print('OPTIONS   :', options)

for opt, arg in options:
    if opt in ('-u', '--user'):
        user = arg
    elif opt in ('-p', '--password'):
        pwd = arg
    elif opt in ('-a', '--appid'): 
        appid = arg
    elif opt in ('-f', '--csvfile'):
        csvfile = arg
    elif opt in ('-h', '--help'):
        print("addDevice_APIv1.py is useful to provisioning the device in the ptNetSuite through the API REST v1")
        print("usage: python addDevice_APIv1.py --user <user> --password <password> --appid <appidNumber> --csvfile </path/to/csvFile.csv>")
        print("options:")
        print("\t-u --user\tusername used to login into the ptNetSuite")
        print("\t-p --password\tpassword  used to login into the ptNetSuite")
        print("\t-a --appid\tappid where you wanto to add the devices")
        print("\t-f --csvfile\tcsv file that contains the devices")
        print("\t-h --help\tthis help")
        exit(0)

print('USER    :', user)
print('PWD     : ***')
print('CSVFILE :', csvfile)


# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/auth/login"
API_ENDPOINT_REMOVE_DEVICES_TMP = "https://" + API_ENDPOINT_ROOT + "/api/v2/device/"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["accessToken"]
print(headers)

print("== REMOVE DEVICES ==")
# open the CVS file
with open(csvfile, 'rt') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='\'')
    head = True
    for row in spamreader:
        # jump the first row
        if head:
            head = False
            continue

        did = row[0]
        API_ENDPOINT_REMOVE_DEVICES = API_ENDPOINT_REMOVE_DEVICES_TMP + did
        print(API_ENDPOINT_REMOVE_DEVICES)
        r = requests.delete(url=API_ENDPOINT_REMOVE_DEVICES, headers=headers)
        print(r.text)

        time.sleep(0.2)
