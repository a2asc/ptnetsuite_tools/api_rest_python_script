#!/usr/bin/python

import csv
import json
import requests
import time
import getopt
import sys


user = ""
pwd = ""

#print('ARGV      :', sys.argv[1:])
options, remainder = getopt.getopt(sys.argv[1:], 'u:p:h', ['user=', 
                                                           'password=',
                                                           'help',
                                                         ])
#print('OPTIONS   :', options)

for opt, arg in options:
    if opt in ('-u', '--user'):
        user = arg
    elif opt in ('-p', '--password'):
        pwd = arg
    elif opt in ('-h', '--help'):
        print("getApps_APIv1.py is useful to provisioning the app in the ptNetSuite through the API REST v1")
        print("usage: python getApps_APIv1.py --user <user> --password <password>")
        print("options:")
        print("\t-u --user\tusername used to login into the ptNetSuite")
        print("\t-p --pasword\tpassword  used to login into the ptNetSuite")
        print("\t-h --help\tthis help")
        exit(0)

print('USER    :', user)
print('PWD     : ***')

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/auth/login"
API_ENDPOINT_GET_APPS = "https://" + API_ENDPOINT_ROOT + "/api/mqtt/v1/users/" + user + "/apps"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["accessToken"]
print(headers)

#get device
print("== GET APPS ==")
r = requests.get(url=API_ENDPOINT_GET_APPS, headers=headers)
print(r.text)

rJson = json.loads(r.text)


result = {}
for restaurant in rJson["reply"]:
    try:
        result["appid"] = restaurant["appid"]
        result["joineui"] = restaurant["joineui"]
        result["label"] = restaurant["label"]
        result["type"] = restaurant["type"]
        print(json.dumps(result))
    except ValueError:
        print("Oops!  That was no valid number.  Try again...")


