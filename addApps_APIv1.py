#!/usr/bin/python

import csv
import json
import requests
import time
import getopt
import sys


user = ""
pwd = ""
appid = ""
csvfile = ""

#print('ARGV      :', sys.argv[1:])
options, remainder = getopt.getopt(sys.argv[1:], 'u:p:a:f:h', ['user=', 
                                                               'password=',
                                                               'appid=',
                                                               'csvfile=',
                                                               'help'
                                                         ])
#print('OPTIONS   :', options)

for opt, arg in options:
    if opt in ('-u', '--user'):
        user = arg
    elif opt in ('-p', '--password'):
        pwd = arg
    elif opt in ('-f', '--csvfile'):
        csvfile = arg
    elif opt in ('-h', '--help'):
        print("addApps_APIv1.py is useful to provisioning the app in the ptNetSuite through the API REST v1")
        print("usage: python addApps_APIv1.py --user <user> --password <password> --csvfile </path/to/csvFile.csv>")
        print("options:")
        print("\t-u --user\tusername used to login into the ptNetSuite")
        print("\t-p --pasword\tpassword  used to login into the ptNetSuite")
        print("\t-f --csvfile\tcsv file that contains the apps")
        print("\t-h --help\tthis help")
        exit(0)

print('USER    :', user)
print('PWD     : ***')
print('CSVFILE :', csvfile)

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/auth/login"
API_ENDPOINT_ADD_APPS = "https://" + API_ENDPOINT_ROOT + "/api/mqtt/v1/users/" + user + "/apps"
login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["accessToken"]
print(headers)

time.sleep(1)

print("== ADD APPS " + appid + " ==")



# open the CVS file
with open(csvfile, 'rt') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='\'')
    head = True
    for row in spamreader:
        # jump the first row
        if head:
            head = False
            continue

        body = {
            "label": "",
            "appid": 1,
            "type": 128,
            "joineui": "",
            "info": {}
}

        #print ', '.join(row)
        appid = row[0]
        joineui = row[1]
        label = row[2]
    
        #print(devEui, devAdr, labelId, firmware, nwkSKey, appSKey)

        body["appid"] = appid
        body["joineui"] = joineui
        body["label"] = label

        print(json.dumps(body))
        r = requests.post(url=API_ENDPOINT_ADD_APPS, headers=headers, data=json.dumps(body))
        print(r.text)
        time.sleep(0.2)
