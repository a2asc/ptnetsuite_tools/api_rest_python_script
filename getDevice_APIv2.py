#!/usr/bin/python

import csv
import json
import requests
import time

user = "---"
pwd = "---"
appid = "---"

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/v2/auth/login"
API_ENDPOINT_GET_DEVICES = "https://" + API_ENDPOINT_ROOT + "/api/v2/devices"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

print(r.text)
# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["token"]
print(headers)

#get device
print("== GET DEVICES from APPID " + appid + " ==")
r = requests.get(url=API_ENDPOINT_GET_DEVICES, headers=headers)
print(r.text)
