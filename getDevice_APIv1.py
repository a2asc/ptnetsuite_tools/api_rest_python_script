#!/usr/bin/python

import csv
import json
import requests
import time
import getopt
import sys


user = ""
pwd = ""
appid = ""

#print('ARGV      :', sys.argv[1:])
options, remainder = getopt.getopt(sys.argv[1:], 'u:p:a:h', ['user=', 
                                                             'password=',
                                                             'appid=',
                                                             'help',
                                                         ])
#print('OPTIONS   :', options)

for opt, arg in options:
    if opt in ('-u', '--user'):
        user = arg
    elif opt in ('-p', '--password'):
        pwd = arg
    elif opt in ('-a', '--appid'): 
        appid = arg
    elif opt in ('-h', '--help'):
        print("getDevice_APIv1.py is useful to provisioning the device in the ptNetSuite through the API REST v1")
        print("usage: python addDevice_APIv1.py --user <user> --password <password> --appid <appidNumber>")
        print("options:")
        print("\t-u --user\tusername used to login into the ptNetSuite")
        print("\t-p --pasword\tpassword  used to login into the ptNetSuite")
        print("\t-a --appid\tappid where you wanto to add the devices")
        print("\t-h --help\tthis help")
        exit(0)

print('USER    :', user)
print('PWD     : ***')
print('APPID   :', appid)

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/auth/login"
API_ENDPOINT_GET_DEVICES = "https://" + API_ENDPOINT_ROOT + "/api/mqtt/v1/users/" + user + "/apps/" + appid + "/devices"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["accessToken"]
print(headers)

#get device
print("== GET DEVICES from APPID " + appid + " ==")
r = requests.get(url=API_ENDPOINT_GET_DEVICES, headers=headers)
#print(r.text)

rJson = json.loads(r.text)


result = {}
for restaurant in rJson["reply"]:
    try:
        result["deveui"] = restaurant["deveui"]
        result["keys"] = restaurant["keys"]
        print(json.dumps(result))
    except ValueError:
        print("Oops!  That was no valid number.  Try again...")


