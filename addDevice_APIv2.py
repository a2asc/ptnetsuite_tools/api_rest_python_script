#!/usr/bin/python

import csv
import json
import requests
import time

user = "---"
pwd = "---"
appid = 1
csvfile = '---'

# defining the api-endpoint

API_ENDPOINT_ROOT = "net.smartcitylab.io"
API_ENDPOINT_LOGIN = "https://" + API_ENDPOINT_ROOT + "/api/v2/auth/login"
API_ENDPOINT_ADD_DEVICES = "https://" + API_ENDPOINT_ROOT + "/api/v2/device"

login = {"username": "", "password": ""}
login["username"] = user
login["password"] = pwd

headers = {
    'Authorization': 'Bearer ..',
    'Content-Type': 'application/json'
}

# sending post request and saving response as response object
print("== LOGIN ==")
r = requests.post(url=API_ENDPOINT_LOGIN, data=login)

# extracting response text
jsonResp = json.loads(r.text)

#headers["Authorization"] = "TOK:" + jsonResp["accessToken"]
headers["Authorization"] = "Bearer " + jsonResp["token"]
print(headers)

body = {
  "appid": 1,
  "DeviceProfileId": 7,
  "deveui": "00000000000000bb",
  "label": "Label of the device 6",
  "type": "ABP"
}

#{"deveui":"00000000123aabbb","appeui":"000000000123aabb","appKey":"0000000000000000000000000123aabb","DeviceProfileId":2,"appid":1,"type":"OTAA","label":"test"}

time.sleep(1)
#id|macVersion|supports32bitFCnt|supportsClassB|supportsClassC|
#--|----------|-----------------|--------------|--------------|
# 1|1.0.2     |false            |false         |false         |
# 2|1.0.2     |true             |false         |false         |
# 3|1.0.2     |false            |false         |true          |
# 4|1.0.2     |true             |false         |true          |
# 5|1.0.3     |false            |false         |false         |
# 6|1.0.3     |true             |false         |false         |
# 7|1.0.3     |false            |true          |false         |
# 8|1.0.3     |true             |true          |false         |
# 9|1.0.3     |false            |false         |true          |
#10|1.0.3     |true             |false         |true          |

print("== ADD DEVICES from APPID " + str(appid) + " ==")

# open the CVS file
with open(csvfile, 'rt') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='\'')
    head = True
    for row in spamreader:
        # jump the first row
        if head:
            head = False
            continue

        #print ', '.join(row)
        devEui = row[1]
        labelId = row[2]
        profile = row[3]
        deviceType = row[4]
        devAddr = row[5]
        appSKey = row[6]
        nwkSKey = row[7]
        appKey = row[8]
        print(devEui, labelId, profile, deviceType, devAddr, appSKey, nwkSKey, appKey)

        body["deveui"] = devEui.lower()
        body["appid"] = appid
        body["type"] = "OTAA"
        body["DeviceProfileId"] = int(profile)
        body["label"] = labelId
        #body["devaddr"] = devAdr
        #body["nwkSKey"] = nwkSKey
        #body["appSKey"] = appSKey
        body["appeui"] = "6d0061632d006901"
        body["appKey"] = appKey.lower()
        
        print(json.dumps(body))
        
        r = requests.post(url=API_ENDPOINT_ADD_DEVICES, headers=headers, data=json.dumps(body))

        print(r.text)
        time.sleep(0.2)
